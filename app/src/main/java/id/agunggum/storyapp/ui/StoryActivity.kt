package id.agunggum.storyapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import id.agunggum.storyapp.data.model.Story
import id.agunggum.storyapp.databinding.ActivityStoryBinding

class StoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStoryBinding.inflate(layoutInflater )
        setContentView(binding.root)
        supportActionBar?.hide()

        setDetail()
    }

    private fun setDetail() {
        val detail = intent.getParcelableExtra<Story>(EXTRA_STORY)

        binding.apply {
            tvName.text = detail?.name
            tvDescription.text = detail?.description
            tvCreatedAt.text = detail?.createdAt
            Glide.with(this@StoryActivity)
                .load(detail?.photoUrl)
                .into(ivStory)
        }
    }

    companion object {
        const val EXTRA_STORY = "extra_story"
    }
}