package id.agunggum.storyapp.ui

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.ViewModelProvider
import id.agunggum.storyapp.R
import id.agunggum.storyapp.data.Resource
import id.agunggum.storyapp.databinding.ActivityLoginBinding
import id.agunggum.storyapp.utils.UserPreferences
import id.agunggum.storyapp.viewmodel.LoginViewModel
import id.agunggum.storyapp.viewmodel.ViewModelFactory

class LoginActivity : AppCompatActivity() {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "token")
    private lateinit var binding: ActivityLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        setupViewModel()
        setupView()
        setupAction()

        playAnimation()
    }

    private fun setupView() {
        loginViewModel.userInfo.observe(this) {
            when (it) {
                is Resource.Success -> {
                    showLoading(false)
                    startActivity(Intent(this, MainActivity::class.java))
                    finishLogin()
                }
                is Resource.Loading -> showLoading(true)
                is Resource.Error -> {
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    showLoading(false)
                }
            }
        }
    }


    private fun finishLogin() {
        Intent(this@LoginActivity, MainActivity::class.java)
            .apply {
                startActivity(this)
                finish()
            }
    }

    private fun setupViewModel() {
        val pref = UserPreferences.getInstance(dataStore)
        loginViewModel = ViewModelProvider(this, ViewModelFactory(pref))[LoginViewModel::class.java]
    }

    private fun setupAction() {
        binding.btnLogin.setOnClickListener {
            if (valid()) {
                val email = binding.edLoginEmail.text.toString()
                val password = binding.edLoginPassword.text.toString()
                loginViewModel.login(email, password)
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.check_input),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        binding.btnRegister.setOnClickListener{
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun valid() =
        binding.edLoginEmail.error == null && binding.edLoginPassword.error == null && !binding.edLoginEmail.text.isNullOrEmpty() && !binding.edLoginPassword.text.isNullOrEmpty()

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }


    private fun playAnimation() {
        binding.apply {
            ObjectAnimator.ofFloat(binding.ivLogin, View.TRANSLATION_Y, -20f, 20f).apply {
                duration = 1500
                repeatCount = ObjectAnimator.INFINITE
                repeatMode = ObjectAnimator.REVERSE
            }.start()

            val login = ObjectAnimator.ofFloat(textView, View.ALPHA, 1f).setDuration(500)
            val etEmail = ObjectAnimator.ofFloat(tilEmail, View.ALPHA, 1f).setDuration(200)
            val etPassword = ObjectAnimator.ofFloat(tilPassword, View.ALPHA, 1f).setDuration(200)
            val btnLogin = ObjectAnimator.ofFloat(btnLogin, View.ALPHA, 1f).setDuration(200)
            val dontHaveAccount = ObjectAnimator.ofFloat(textView2, View.ALPHA, 1f).setDuration(200)
            val registerButton = ObjectAnimator.ofFloat(btnRegister, View.ALPHA, 1f).setDuration(200)

            AnimatorSet().apply {
                playSequentially(
                    login,
                    etEmail,
                    etPassword,
                    btnLogin,
                    dontHaveAccount,
                    registerButton
                )
                start()
            }
        }
    }
}