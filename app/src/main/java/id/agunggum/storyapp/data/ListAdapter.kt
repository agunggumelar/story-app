package id.agunggum.storyapp.data

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.agunggum.storyapp.data.model.Story
import id.agunggum.storyapp.databinding.ItemListBinding
import id.agunggum.storyapp.ui.StoryActivity

class ListAdapter : RecyclerView.Adapter<ListAdapter.ViewHolder>() {
    private val listStory = ArrayList<Story>()

    @SuppressLint("NotifyDataSetChanged")
    fun setStory(story: ArrayList<Story>) {
        listStory.clear()
        listStory.addAll(story)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listStory[position])
    }

    override fun getItemCount(): Int = listStory.size

    inner class ViewHolder(private val binding: ItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(story: Story) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(story.photoUrl)
                    .into(binding.ivStory)
                tvFirstName.text = story.name?.first().toString()
                tvNama.text = story.name
                tvCreatedAt.text = story.createdAt
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, StoryActivity::class.java).apply {
                    putExtra(StoryActivity.EXTRA_STORY, story)
                }
                val optionsCompat: ActivityOptionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                        itemView.context as Activity,
                        Pair(binding.ivStory, "image"),
                        Pair(binding.tvNama, "name")
                    )
                it.context.startActivity(intent, optionsCompat.toBundle())
            }
        }
    }
}