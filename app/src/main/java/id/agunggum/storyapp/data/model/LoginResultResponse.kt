package id.agunggum.storyapp.data.model

data class LoginResultResponse(
    var nama: String,
    var token: String,
    var userId: String
)