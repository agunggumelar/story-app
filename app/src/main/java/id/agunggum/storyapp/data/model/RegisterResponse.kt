package id.agunggum.storyapp.data.model

data class RegisterResponse(
    val error: Boolean,
    val message: String
)