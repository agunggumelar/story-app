package id.agunggum.storyapp.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StoryList(
    var id: String,
    var name: String,
    var description: String,
    var photoUrl: String,
    var createdAt: String
): Parcelable