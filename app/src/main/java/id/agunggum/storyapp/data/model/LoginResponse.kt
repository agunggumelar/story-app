package id.agunggum.storyapp.data.model

data class LoginResponse(
    val error: Boolean,
    val loginResult: LoginResultResponse,
    val message: String
)