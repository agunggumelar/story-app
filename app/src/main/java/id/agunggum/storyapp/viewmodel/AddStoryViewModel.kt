package id.agunggum.storyapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import id.agunggum.storyapp.data.Resource
import id.agunggum.storyapp.data.model.AddStoryResponse
import id.agunggum.storyapp.data.remote.ApiConfig
import id.agunggum.storyapp.utils.UserPreferences
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddStoryViewModel(private val pref: UserPreferences) : ViewModel() {
    private val _uploadStory = MutableLiveData<Resource<String>>()
    val uploadStory: LiveData<Resource<String>> = _uploadStory

    suspend fun uploadImage(
        imageMultipart: MultipartBody.Part, description: RequestBody,
    ) {
        _uploadStory.postValue(Resource.Loading())
        val client = ApiConfig.getApiClient().uploadImage(
            auth = "Bearer ${pref.getToken().first()}",
            imageMultipart,
            description
        )

        client.enqueue(object : Callback<AddStoryResponse> {
            override fun onResponse(
                call: Call<AddStoryResponse>,
                response: Response<AddStoryResponse>
            ) {
                if (response.isSuccessful) {
                    _uploadStory.postValue(Resource.Success(response.body()?.message))
                } else {
                    val errorResponse = Gson().fromJson(
                        response.errorBody()?.charStream(),
                        AddStoryResponse::class.java
                    )
                    _uploadStory.postValue(Resource.Error(errorResponse.message))
                }
            }

            override fun onFailure(call: Call<AddStoryResponse>, t: Throwable) {
                Log.e(
                    AddStoryResponse::class.java.simpleName,
                    "onFailure upload"
                )
                _uploadStory.postValue(Resource.Error(t.message))
            }
        })
    }
}